import { createStackNavigator } from 'react-navigation';
import Welcome from './screens/Welcome';
import Login from './screens/Login';
import SignUp from './screens/SignUp';
import Filter from './screens/Filter';
import AddProjects from './screens/AddProjects';
import Projects from './screens/Projects';
import ProjectDetails from './screens/ProjectDetails';

const AppNavigator = createStackNavigator({
  Welcome: { screen: Welcome },
  Login: { screen: Login},
  SignUp: { screen: SignUp},
  Filter: {screen: Filter},
  AddProjects: {screen: AddProjects},
  Projects: { screen: Projects,
    navigationOptions: {
      header: null
    }
  },
  ProjectDetails: { screen: ProjectDetails},
});

export default AppNavigator;