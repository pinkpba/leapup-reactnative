import React from 'react';
import { StyleSheet,Text,View,Button,TouchableOpacity,CheckBox,Alert,Header,AsyncStorage,TextInput} from 'react-native';

//const key='@simi:key'


class HomeScreen extends React.Component {
/* static navigationOptions={
    title:"Project",
    alignSelf: 'flex-end',
    alignText:"center",
    backgroundColor:"blue",

}*/
static navigationOptions = () => ({
  title: 'Projects',
  headerTitleStyle: { 
  textAlign: 'center',
  flex:1,
  color:'white', 
  },
  headerStyle: {
  backgroundColor: 'rgb(28, 76, 121)'
  },
  headerLeft: null,
  });

   constructor(){
    super();
    this.state={
      check:false,
      check1:false,
      check2:false,
      check3:false,
      check4:false,
      check5:false,
      text:'',
      storedValue:''

    }
  }
/*componentWillMount(){
  this.onLoad();
}*/
onChange=(text)=>{
  this.setState({text});
}


  checkBoxTest(){
    this.setState({
     check:!this.state.check
    })

  }
  checkBox1(){
   this.setState({
     check1:!this.state.check1
   })
  }
 
  checkBox2(){
    this.setState({
      check2:!this.state.check2
    })
   }
   checkBox3(){
    this.setState({
      check3:!this.state.check3
    })
   }
   checkBox4(){
    this.setState({
      check4:!this.state.check4
    })
   }
   checkBox5(){
    this.setState({
      check5:!this.state.check5
    })
   }
  _onPressButton(){
    Alert.alert('Applied')

  }



  render() {
    return (
        <View style={styles.Container} >
    
          {/*<Text style={{ textAlign: 'center',color:'white',marginTop:20,fontSize:20}}>Projects</Text>
        style={{paddingTop: 20, paddingBottom: 20,backgroundColor:'white'}}*/}
  
      
         <View style={styles.checkbox}>
           <Text style={styles.chckbxhd}> ShowBy</Text>
          

           <View style={styles.chckbx}> 
             <CheckBox value={this.state.check} onChange={()=>this.checkBoxTest() } />
             <Text style={styles.chckbxtxt}>All my projects</Text>
             </View>
           
             <View style={styles.chckbx}>
                <CheckBox value={this.state.check1} onChange={()=>this.checkBox1()}
           /><Text style={styles.chckbxtxt}>Owned by me</Text>
               </View>
         
            <View style={styles.chckbx}> 
            <CheckBox value={this.state.check2} onChange={()=>this.checkBox2()}
           /><Text style={styles.chckbxtxt}>Completed</Text>
            </View>
          
           
          </View>
          
          <View
    style={{
      marginTop:30,
     borderBottomColor:"rgba(207,208,208,1)",
      borderBottomWidth: 1,
    }}
  />
  
  <View style={styles.checkbox}>
    <Text style={styles.chckbxhd}> SortBy</Text>
            <View style={styles.chckbx} > 
            <CheckBox value={this.state.check3} onChange={()=>this.checkBox3()}
           /><Text style={styles.chckbxtxt}>Last updated</Text>
            </View>

           <View style={styles.chckbx}>
           <CheckBox value={this.state.check4} onChange={()=>this.checkBox4()}
           /><Text style={styles.chckbxtxt}>Priority</Text>
           </View>

           <View style={styles.chckbx}>
           <CheckBox value={this.state.check5} onChange={()=>this.checkBox5()}
           /><Text style={styles.chckbxtxt}>Milestone</Text>
           </View>

      </View>   
 
    {/*<TouchableOpacity  onPress={this._onPressButton} style={styles.aplybtn}>
        <Text style={styles.Aplytxt}>Apply</Text>

  </TouchableOpacity>*/}
  <View style={styles.btnstyle}>
                <Button title="Apply" onPress={() =>
                this.props.navigation.navigate('Projects')
                }/>
            </View>
   
</View>
    );
  }
  
}
export default HomeScreen;
const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
     paddingBottom: 20,
     backgroundColor:'white'
  },
  checkbox:{
    marginTop:20,
    marginLeft:50
  },
  chckbxhd:{
    fontWeight:"bold",
    color:"black",
    
    fontSize:20
  },
  chckbx:{
    flexDirection:'row'
  },
  chckbxtxt:{
    marginTop:5,
    fontSize:15
  },
  aplybtn:{
    height: 38,
    width:113,
    marginTop: 40 ,
    backgroundColor: "rgba(0,172,238,1)",
    left:130,
    borderRadius:2,
    alignItems:'center',
    justifyContent:'center'
  },
  Aplytxt:{
    color:"rgba(255,255,255,1)"
  },
  btnstyle: {
    alignItems:'center',
    justifyContent:'center',
}
});
        

