import React from 'react';
import { View, Text,Image,StyleSheet,TouchableOpacity } from 'react-native';
import {createStackNavigator,createBottomTabNavigator} from 'react-navigation';

//import { FloatingAction } from 'react-native-floating-action';
import ProgressCircle from 'react-native-progress-circle';

//ProScreen

class ProScreen extends React.Component {

  render() {
      return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.proView} onPress={() => this.props.navigation.navigate('ProjectDetails')}> 
                <View  style={styles.pjtView}>
                    <View style={styles.pjtTxtVew}>
                        <Text style={styles.pjtTxt}>Real-time PCR App -Android</Text>
                        <Text>Madhu,Lini,Nadesan</Text>
                        <Text>3 hr ago</Text>
                        <Text>Medium <Image source={require('./images/assets/minorissue.png')} style={styles.icon}/>  4minor issues</Text>
                    </View>
                    <View style={styles.pgrsCrcle}>
                        <ProgressCircle style={{marginTop:10}} percent={75} radius={30} borderWidth={8} color="#3399FF" shadowColor="#999" bgColor="#fff">
                            <Text style={{ fontSize: 18 }}>{'75%'}</Text>
                        </ProgressCircle>
                    </View>
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.proView} onPress={() => this.props.navigation.navigate('Login')}> 
                <View  style={styles.pjtView}>
                    <View style={styles.pjtTxtVew}>
                        <Text style={styles.pjtTxt}>Snap</Text>
                        <Text>Madhu,Lini,Nadesan</Text>
                        <Text>3 hr ago</Text>
                        <Text>High <Image source={require('./images/assets/minorissue.png')} style={styles.icon}/>  2 major issues</Text>
                    </View>
                    <View style={styles.pgrsCrcle}>
                        <ProgressCircle style={{marginTop:10}} percent={20} radius={30} borderWidth={8} color="#3399FF" shadowColor="#999" bgColor="#fff">
                            <Text style={{ fontSize: 18 }}>{'20%'}</Text>
                        </ProgressCircle>
                    </View>
                </View>
            </TouchableOpacity>

           <TouchableOpacity style={styles.proView} onPress={() => this.props.navigation.navigate('filter')}> 
                <View  style={styles.pjtView}>
                    <View style={styles.pjtTxtVew}>
                        <Text style={styles.pjtTxt}>Leapup mobile app - Android</Text>
                        <Text>Madhu,Lini,Nadesan</Text>
                        <Text>3 hr ago</Text>
                        <Text>Medium <Image source={require('./images/assets/minorissue.png')} style={styles.icon}/>  1 minor issues</Text>
                    </View>
                    <View style={styles.pgrsCrcle}>
                        <ProgressCircle style={{marginTop:10}} percent={100} radius={30} borderWidth={8} color="#3399FF" shadowColor="#999" bgColor="#fff">
                            <Text style={{ fontSize: 18 }}>{'100%'}</Text>
                        </ProgressCircle>
                    </View>
                </View>
            </TouchableOpacity>

            {/*<View style={styles.floatIcon}>
                <FloatingAction color="rgba(0,172,238,1)" 
                 onPressItem={() => this.props.navigation.navigate('ProjectDetails')} />
        </View>
        <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2017/11/Floating_Button.png'}} */}

        <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('ProjectDetails')} style={styles.TouchableOpacityStyle} >
             <Image source={require('./images/assets/plusIcon.png')}style={styles.FloatingButtonStyle} />
         </TouchableOpacity>
        </View>
      );
  }
}
//TodoScreen

class TodoScreen extends React.Component {
 
    render() {
        return (
            <View style={styles.todoView}>
                <Text>Todo!</Text>
               
            </View>
        );
    }
}

//FeedScreen

class FeedScreen extends React.Component {

  render() {
      return (
          <View style={styles.feedView}>
              <Text>Feeds</Text>
          </View>
      );
  }
}
//Milescreen
class MileScreen extends React.Component {
 
  render() {
      return (
          <View style={styles.milestoneView}>
              <Text>Milestone</Text>
          </View>
      );
  }
}
//profileScreen
class ProfileScreen extends React.Component {
    
  render() {
      return (
          <View style={styles.profileView}>
              <Text>Profile</Text>
          </View>
      );
  }
}


//project starts
const ProStack = createStackNavigator({ ProScreen }, {
  navigationOptions: {
      title: 'Projects',
      headerTitleStyle: { 
          textAlign: 'center',
          justifyContent:'center',
          flex:2,
          color:'white', 
      },
      headerStyle: {
           backgroundColor: 'rgb(28, 76, 121)'
      },
      headerLeft: (
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Welcome')}>
              <Image source={require('./images/backarrow.png')} />
          </TouchableOpacity>
      ),
      headerRight: (
          <TouchableOpacity /*style={styles.back}*/ style={{position:'absolute',top:15,right:15}}onPress={() => this.props.navigation.navigate('Welcome')}>
              <Image  source={require('./images/assets/filter.png')} />
          </TouchableOpacity>
      ),  
  },

})
  ProStack.navigationOptions = {
      tabBarLabel: 'Projects',
      tabBarIcon: ({ tintColor }) => (
         <Image source={require('./images/assets/project.png')} style={[styles.icon, { tintColor: tintColor }]}/>
      ),   
  };

//todo starts
const TodoStack = createStackNavigator({ TodoScreen }, {
  navigationOptions: {
      title: 'Todo',
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1,
        color:'white', 
      },
      headerStyle: {
         backgroundColor: 'rgb(28, 76, 121)'
      },
      headerLeft: null,
  },

})


TodoStack.navigationOptions = {
    tabBarLabel: 'Todo',
    tabBarIcon: ({ tintColor }) => (
        <Image source={require('./images/assets/todo.png')} style={[styles.icon, { tintColor: tintColor }]}/>
    ),  
};
//feeds start

const FeedStack = createStackNavigator({ FeedScreen }, {
  navigationOptions: {
      title: 'Feeds',
      headerTitleStyle: { 
          textAlign: 'center',
          flex:1,
          color:'white', 
      },
      headerStyle: {
        backgroundColor: 'rgb(28, 76, 121)'
      },
       headerLeft: null,
  },

})


FeedStack.navigationOptions = {
      tabBarLabel: 'Feeds',
      tabBarIcon: ({ tintColor }) => (
          <Image source={require('./images/assets/feed.png')} style={[styles.icon, { tintColor: tintColor }]}/>
      ),
};

//milestone start
const MileStack = createStackNavigator({ MileScreen }, {
  navigationOptions: {
      title: 'Milestone',
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1,
        color:'white', 
      },
      headerStyle: {
         backgroundColor: 'rgb(28, 76, 121)'
      },
      headerLeft: null,
  },

})


MileStack.navigationOptions = {
      tabBarLabel: 'Milestone!',
      tabBarIcon: ({ tintColor }) => (
          <Image source={require('./images/assets/milestone.png')} style={[styles.icon, { tintColor: tintColor }]} />
      ),
};
//profile starts
const ProfileStack = createStackNavigator({ ProfileScreen }, {
  navigationOptions: {
      title: 'profile',
      headerTitleStyle: { 
          textAlign: 'center',
          flex:1,
          color:'white', 
      },
      headerStyle: {
        backgroundColor: 'rgb(28, 76, 121)'
      },
       headerLeft: null,
  },

})


ProfileStack.navigationOptions = {
      tabBarLabel: 'profile',
      tabBarIcon: ({ tintColor }) => (
           <Image source={require('./images/assets/profile.png')} style={[styles.icon, { tintColor: tintColor }]}/>
      ),
};

export default createBottomTabNavigator({
  ProStack,
  TodoStack,
  FeedStack,
  MileStack,
  ProfileStack,
});

const styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor:'#f3f5f6',
  },
  /*floatIcon: {
    flex:1,
  },*/

  TouchableOpacityStyle:{
 
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
 
  FloatingButtonStyle: {
 
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },

  icon: {
    width: 20,
    height: 20,
  },

  proView:{
    backgroundColor:'white',
    padding:10,
    
    borderBottomColor:"rgba(207,208,208,1)",
    borderBottomWidth:1,
  },

  pjtView:{
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  pjtTxtVew:{
    marginTop:15
  },

  pjtTxt:{
    color:'black',
    fontWeight:'bold',
    fontFamily:'helvetica',
    fontSize:20 
  },

  pgrsCrcle:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },

  todoView:{
    flex: 1, 
    justifyContent: 'center',
     alignItems: 'center'
  },

  feedView:{
    flex: 1, 
    justifyContent: 'center',
     alignItems: 'center'
  },

  milestoneView:{
    flex: 1, 
    justifyContent: 'center',
     alignItems: 'center'
  },

  profileView:{
    flex: 1, 
    justifyContent: 'center',
     alignItems: 'center'
  },
 
});