import React, {Component} from 'react';
import { StyleSheet, Button, View, ImageBackground,Text, Image, TouchableOpacity, TextInput } from 'react-native';

import RadioGroup from 'react-native-radio-buttons-group';

export default class AddPro extends Component {
    static navigationOptions = () => ({
        title: 'Add Projects',
        headerTitleStyle: { 
            textAlign: 'center',
            flex:1,
            color:'white', 
        },
        headerStyle: {
            backgroundColor: 'rgb(28, 76, 121)'
        },
        headerLeft: null,
    });

    state = {
        data: [
            {
                label: 'High',
                size:20,
                color:'rgba(0,172,238,1)',
            },
            {
                label: 'Medium',
                size:20,
                color:'rgba(0,172,238,1)',
            },
            {
                label: 'Low',
                size:20,
                color:'rgba(0,172,238,1)',
            },
        ],
    };

    // update state
    onPress = data => this.setState({ data });

  render() {
    let selectedButton = this.state.data.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
    return (
        <View style={styles.container}>
            <TextInput placeholder="Project Name" underlineColorAndroid='transparent' style={styles.inputText}/>
            <TextInput  multiline={true} textAlignVertical={'top'} placeholder="Description" underlineColorAndroid='transparent' style={[styles.inputText,styles.inputTxt]}/>
            <TextInput placeholder="Team Members" underlineColorAndroid='transparent' style={styles.inputText}/>

            <View style={styles.radiocontainer}>
                <Text style={styles.radiotext}>Priority</Text>
                <RadioGroup radioButtons={this.state.data} onPress={this.onPress} flexDirection='row' />
            </View>

            <View style={styles.btnstyle}>
                <Button title="Add Projects" onPress={() =>
                this.props.navigation.navigate('Projects')
                }/>
            </View>
            
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        padding:20,
    },
    inputText: {
        marginTop:10,
        marginBottom:10,
        height:60,
        color:'rgba(99,116,135,1)',
        paddingHorizontal:10,
        borderWidth:0.5,
        borderColor:'rgba(128,145,164,1)',
        borderRadius:3,
    },
    inputTxt: {
        height:120,
    },
    radiocontainer: {
        marginBottom:40,
    },
    radiotext: {
        fontSize: 16,
        marginTop: 15,
    },
    btnstyle: {
        alignItems:'center',
        justifyContent:'center',
    }
});