import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';

export default class LoginForm extends Component {
    render(){
        return(
            <View style={styles.container}>
                <TextInput placeholder="E-mail" underlineColorAndroid='transparent' style={styles.inputText}/>
                <TextInput placeholder="Password" underlineColorAndroid='transparent' style={[styles.inputText,styles.inputTxt]}/>

                <Button title="Login" onPress={() =>
                this.props.navigation.navigate('../Welcome')
                }/>
                <Text style={styles.fgtpwd}>Forgot Password?</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding:20
    },
    inputText: {
        height:60,
        backgroundColor:'rgba(212,233,255,1)',
        color:'rgba(99,116,135,1)',
        paddingHorizontal:10,
    },
    inputTxt: {
        borderTopWidth:0.5,
        borderColor:'rgba(128,145,164,1)',
        marginBottom:30,
    },
    fgtpwd:{
        color:'rgba(0,172,238,1)',
        fontSize:13,
        textAlign:'center',
        marginTop:20,
    }
});