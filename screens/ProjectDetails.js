import React from 'react';
import { Text, View } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';

class OverviewScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Overview!</Text>
      </View>
    );
  }
}

class PingScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Ping!</Text>
      </View>
    );
  }
}

class NotesScreen extends React.Component {
    render() {
        return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Notes!</Text>
        </View>
        );
    }
}

class MilestonesScreen extends React.Component {
    render() {
        return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Milestones!</Text>
        </View>
        );
    }
}

export default createMaterialTopTabNavigator({
  Overview: OverviewScreen,
  Ping: PingScreen,
  Notes:NotesScreen,
  Milestones:MilestonesScreen,
});