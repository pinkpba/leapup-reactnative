import React, {Component} from 'react';
import { StyleSheet, Button, View, ImageBackground,Text, Image, TouchableOpacity } from 'react-native';

import Swiper from 'react-native-swiper';

export default class Home extends Component {
  static navigationOptions= {
    header: null,
  };
  render() {
    return (
      <ImageBackground source={require('./images/bg.png')} style={styles.container}>

        <Image source={require('./images/one.png')} />

        <Text style={styles.swipetext}>Track & {"\n"}monitor projects</Text>

        <View style={styles.btncontainer}>
            <TouchableOpacity style={styles.buttonContainer} onPress={() =>
              this.props.navigation.navigate('Login')
            }><Text style={styles.btntxt}>Login</Text></TouchableOpacity>

            <TouchableOpacity style={[styles.buttonContainer,styles.btntext]} onPress={() =>
              this.props.navigation.navigate('SignUp')
            }><Text  style={styles.btntxt}>Sign up</Text></TouchableOpacity>
        </View>

        {/*<Swiper style={styles.wrapper} showsButtons={true}>
          <View style={styles.slide1}>
            <Text style={styles.text}>Hello Swiper</Text>
          </View>
          <View style={styles.slide2}>
            <Text style={styles.text}>Beautiful</Text>
          </View>
          <View style={styles.slide3}>
            <Text style={styles.text}>And simple</Text>
          </View>
        </Swiper>*/}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  swipetext : {
    fontSize:20,
    textAlign:'center',
    color:'rgba(255,255,255,1)',
  },
  btncontainer: {
    position:'absolute',
    bottom:0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainer: {
    flex: 1,
    alignItems:'center',
    paddingVertical:20,
    borderTopWidth:0.5,
    borderColor:'#C0C0C0',
  },
  btntxt: {
    color:'rgba(255,255,255,1)',
    fontSize:16,
  },
  btntext: {
    borderLeftWidth:0.5,
    borderColor:'#C0C0C0',
  }
});