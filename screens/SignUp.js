import React, {Component} from 'react';
import { StyleSheet, Button, View, ImageBackground,Text, Image, TouchableOpacity, TextInput } from 'react-native';

export default class SignUp extends Component {
  static navigationOptions= {
    header: null,
  };
  render() {
    return (
      <ImageBackground source={require('./images/bg.png')} style={styles.container}>
        <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.navigate('Welcome')}>
          <Image  source={require('./images/backarrow.png')} />
        </TouchableOpacity>

        <View style={styles.logo}>
          <Image source={require('./images/logo.png')} />
        </View>
       

        <View style={styles.logincontainer}>
            <TextInput placeholder="Name" underlineColorAndroid='transparent' style={styles.inputText}/>
            <TextInput placeholder="E-mail" underlineColorAndroid='transparent' style={[styles.inputText,styles.inputTxt]}/>
            <TextInput placeholder="Password" underlineColorAndroid='transparent' style={[styles.inputText,styles.finalInput]}/>

            <Button title="Get Started" onPress={() =>
            this.props.navigation.navigate('Filter')
            }/>
        </View>

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  back: {
    position:'absolute',
    top:15,
    left:15,
  },
  logo: {
    alignItems:'center',
    justifyContent:'center',
    flexGrow:1
  },
  logincontainer: {
    padding:20
  },
  inputText: {
    height:60,
    backgroundColor:'rgba(212,233,255,1)',
    color:'rgba(99,116,135,1)',
    paddingHorizontal:10,
},
inputTxt: {
    borderTopWidth:0.5,
    borderBottomWidth:0.5,
    borderColor:'rgba(128,145,164,1)',
},
finalInput: {
    marginBottom:30,
}
});